package observers

import (
	"os"
	"io/fs"
	"path/filepath"

	"backup/logging"
	"backup/common"
)

var Stat = os.Stat

type DirectoryScanner struct {
	hotDirPath    string
	backupDirPath string
	workChan      chan string
	logger        logging.FileLogger
}

func CreateDirectoryScanner(hotDirPath, backupDirPath string, logger logging.FileLogger, workChan chan string) DirectoryScanner {
	logger.SetPrefix("DirectoryScanner")
	return DirectoryScanner{
		hotDirPath:    hotDirPath,
		backupDirPath: backupDirPath,
		workChan:      workChan,
		logger:        logger,
	}
}

func (scanner *DirectoryScanner) Start() {
	scanner.logger.Info("Start scanning directory : " + scanner.hotDirPath)

	files, err := os.ReadDir(scanner.hotDirPath)
	if err != nil {
		scanner.logger.Error("Scanner error: " + err.Error())
		return
	}

	for _, file := range files {
		if file.Type().IsRegular() {
			fileInfo, err := file.Info()
			if err != nil {
				scanner.logger.Error("Error while getting FileInfo(): " + err.Error())
				continue
			}

			scanner.checkForWork(fileInfo)
		}
	}

	scanner.logger.Info("Scanning is finished")
}

func (scanner *DirectoryScanner) isWorkNeeded(originalFile fs.FileInfo) (bool, error) {
	if common.IsDeleteNeeded(originalFile.Name()) {
		return true, nil
	}

	backedupFile, err := Stat(filepath.Join(scanner.backupDirPath, originalFile.Name() + ".bak"))
	if err != nil {
		if os.IsNotExist(err) {
			return true, nil
		}
		return false, err
	}

	return backedupFile.ModTime().Before(originalFile.ModTime()), nil
}

func (scanner *DirectoryScanner) checkForWork(fileInfo fs.FileInfo) {
	if isNeeded, err := scanner.isWorkNeeded(fileInfo); err != nil {
		scanner.logger.Error("Scanner error: " + err.Error())
	} else if isNeeded {
		scanner.logger.Info("Detected work to do with " + fileInfo.Name())
		scanner.workChan <- fileInfo.Name()
	}
}
