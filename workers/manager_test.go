package workers

import (
	"testing"

	"backup/common"
	"backup/logging"
)

func checkWorkFromChannel(t *testing.T, manager *Manager, expected *common.Work) {
	if expected == nil {
		if len(manager.workerChan) != 0 {
			t.Fatalf(`No work in channel expected`)
		}

		return
	}

	work := <-manager.workerChan
	if work.Operation != expected.Operation {
		t.Fatalf(`Operation from channel, actual=%d, expected=%d`, work.Operation, expected.Operation)
	}

	if work.FileName != expected.FileName {
		t.Fatalf(`File name from channel isn't right, filename=%s, expected=%s`, work.FileName, expected.FileName)
	}
}

func checkCopyWork(t *testing.T, manager *Manager, fileName string) {
	work := <-manager.workerChan
	if work.Operation != common.Copy {
		t.Fatalf(`Operation from channel isn't Copy, operation=%d`, work.Operation)
	}

	if work.FileName != fileName {
		t.Fatalf(`File name from channel isn't right, filename=%s, expected=%s`, work.FileName, fileName)
	}
}

func checkFileInfo(t *testing.T, manager *Manager, fileName string, expFileInfo FileInfo) {
	fileInfo, ok := manager.filesStorage[fileName]

	if !ok {
		t.Fatalf(`filesStorage for %s should exist`, fileName)
	}

	if fileInfo.OperationOngoing != expFileInfo.OperationOngoing {
		t.Fatalf(`OperationOngoing for %s should be %t`, fileName, expFileInfo.OperationOngoing)
	}

	if fileInfo.NextOperation != expFileInfo.NextOperation {
		t.Fatalf(`NextOperation for %s should be %d`, fileName, expFileInfo.NextOperation)
	}

	if fileInfo.PendingDeletion != expFileInfo.PendingDeletion {
		t.Fatalf(`PendingDeletion for %s should be %t`, fileName, expFileInfo.PendingDeletion)
	}
}

func TestGetNameWithoutPrefix(t *testing.T) {
	logger := logging.CreateSilentLogger()
	incomingWorkChan := make(chan string, common.CHANNEL_SIZE)
	manager := CreateManager(logger, incomingWorkChan)

	testValues := []struct {
		Filename string
		Expected string
	}{
		{
			"delete_filename.cpp",
			"filename.cpp",
		},
		{
			"delete_2023-02-02T18:54:30+02:00_filename.cpp",
			"filename.cpp",
		},
		{
			"filename.cpp",
			"filename.cpp",
		},
		{
			"_2023-02-02T18:54:30+02:00_filename.cpp",
			"_2023-02-02T18:54:30+02:00_filename.cpp",
		},
	}

	for _, values := range testValues {

		cutName := manager.getNameWithoutPrefix(values.Filename)
		if cutName != values.Expected {
			t.Fatalf(`GetNameWithoutPrefix return=%s, expected=%s`, cutName, values.Expected)
		}
	}
}

func TestIsPendingDeleteNeeded(t *testing.T) {
	logger := logging.CreateSilentLogger()
	incomingWorkChan := make(chan string, common.CHANNEL_SIZE)
	manager := CreateManager(logger, incomingWorkChan)

	testValues := []struct {
		Filename string
		Expected bool
	}{
		{
			"delete_2024-02-02T18:54:30+02:00_filename.cpp",
			true,
		},
		{
			"delete_2124-02-02T18:54:30+02:00_filename.cpp",
			true,
		},
		{
			"delete_2124/02/02T18:54:30+02:00_filename.cpp",
			false,
		},
		{
			"delete_filename.cpp",
			false,
		},
		{
			"filename.cpp",
			false,
		},
		{
			"_2023-02-02T18:54:30+02:00_filename.cpp",
			false,
		},
	}

	for _, values := range testValues {

		result := manager.isPendingDeleteNeeded(values.Filename)
		if result != values.Expected {
			t.Fatalf(`IsPendingDeleteNeeded return=%t, expected=%t`, result, values.Expected)
		}
	}
}

func TestSendWork(t *testing.T) {
	logger := logging.CreateSilentLogger()
	incomingWorkChan := make(chan string, common.CHANNEL_SIZE)
	manager := CreateManager(logger, incomingWorkChan)

	fileName := "fileName.cpp"
	fileInfo := FileInfo{PendingDeletion: false, OperationOngoing: false, NextOperation: common.None}
	manager.filesStorage[fileName] = &fileInfo

	manager.sendWork(&fileInfo, fileName, common.Copy)
	checkFileInfo(t, &manager, fileName, FileInfo{false, true, common.None})

	if len(manager.workerChan) == 1 {
		checkCopyWork(t, &manager, fileName)
	} else {
		t.Fatalf(`Wrong number operations in channel, number=%d`, len(manager.workerChan))
	}

	manager.sendWork(&fileInfo, fileName, common.Copy)
	checkFileInfo(t, &manager, fileName, FileInfo{false, true, common.Copy})

	if len(manager.workerChan) != 0 {
		t.Fatalf(`Wrong number operations in channel, number=%d`, len(manager.workerChan))
	}
}

func TestHandleDeleteResult(t *testing.T) {
	logger := logging.CreateSilentLogger()
	incomingWorkChan := make(chan string, common.CHANNEL_SIZE)
	manager := CreateManager(logger, incomingWorkChan)

	manager.filesStorage["filename2.cpp"] = &FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.None}
	manager.filesStorage["filename3.cpp"] = &FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.None}
	manager.filesStorage["filename4.cpp"] = &FileInfo{PendingDeletion: false, OperationOngoing: false, NextOperation: common.None}
	manager.filesStorage["filename5.cpp"] = &FileInfo{PendingDeletion: false, OperationOngoing: false, NextOperation: common.Copy}

	resultValue := []struct {
		keyName string
		result  common.Work
	}{
		{"filename2.cpp", common.Work{"delete_filename2.cpp", common.Delete}},
		{"filename3.cpp", common.Work{"delete_2024-02-02T18:54:30+02:00_filename3.cpp", common.Delete}},
		{"filename4.cpp", common.Work{"delete_filename4.cpp", common.Delete}},
		{"filename5.cpp", common.Work{"delete_2024-02-02T18:54:30+02:00_filename5.cpp", common.Delete}},
		{"filename6.cpp", common.Work{"delete_filename6.cpp", common.Delete}},
	}

	for _, val := range resultValue {
		manager.handleResult(val.result)

		_, ok := manager.filesStorage[val.keyName]

		if ok {
			t.Fatalf(`filesStorage for %s should have been deleted`, val.keyName)
		}
	}
}

func TestHandleCopyResult(t *testing.T) {
	logger := logging.CreateSilentLogger()
	incomingWorkChan := make(chan string, common.CHANNEL_SIZE)
	manager := CreateManager(logger, incomingWorkChan)

	fileName := "filename2.cpp"
	manager.filesStorage[fileName] = &FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.None}
	manager.handleResult(common.Work{fileName, common.Copy})

	checkFileInfo(t, &manager, fileName, FileInfo{false, false, common.None})

	fileName = "filename3.cpp"
	manager.filesStorage[fileName] = &FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.Copy}
	manager.handleResult(common.Work{fileName, common.Copy})

	checkFileInfo(t, &manager, fileName, FileInfo{false, true, common.None})

	if len(manager.workerChan) == 1 {
		checkCopyWork(t, &manager, fileName)
	} else {
		t.Fatalf(`Wrong number operations in channel, number=%d`, len(manager.workerChan))
	}
}

func TestDesernWorkWithWrongDeleteNames(t *testing.T) {
	logger := logging.CreateSilentLogger()
	incomingWorkChan := make(chan string, common.CHANNEL_SIZE)
	manager := CreateManager(logger, incomingWorkChan)

	wrongDeleteNames := []struct {
		fileName       string
		deleteFileName string
	}{
		{"filename1.cpp", "Delete_filename1.cpp"},
		{"filename2.cpp", "deletefilename2.cpp"},
		{"filename3.cpp", "DELETE_filename3.cpp"},
		{"filename4.cpp", "delete=filename4.cpp"},
	}

	for _, values := range wrongDeleteNames {
		deleteFileName := values.deleteFileName

		manager.filesStorage[values.fileName] = &FileInfo{PendingDeletion: false, OperationOngoing: false, NextOperation: common.None}

		manager.discernWork(deleteFileName)

		checkFileInfo(t, &manager, deleteFileName, FileInfo{false, true, common.None})

		if len(manager.workerChan) == 1 {
			checkCopyWork(t, &manager, deleteFileName)
		} else {
			t.Fatalf(`Wrong number operations in channel, number=%d`, len(manager.workerChan))
		}
	}
}

func TestDesernWork(t *testing.T) {
	logger := logging.CreateSilentLogger()
	incomingWorkChan := make(chan string, common.CHANNEL_SIZE)
	manager := CreateManager(logger, incomingWorkChan)

	preCreactedValues := []struct {
		fileName string
		fileInfo *FileInfo
	}{
		{"filename0.txt", &FileInfo{PendingDeletion: false, OperationOngoing: false, NextOperation: common.None}},
		{"filename1.txt", &FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.None}},
		{"filename2.txt", &FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.Copy}},
		{"filename3.txt", &FileInfo{PendingDeletion: true, OperationOngoing: false, NextOperation: common.None}},
		{"filename4.txt", &FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.None}},
		{"filename5.txt", &FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.Copy}},
		{"filename6.txt", &FileInfo{PendingDeletion: false, OperationOngoing: false, NextOperation: common.None}},
		{"filename7.txt", &FileInfo{PendingDeletion: false, OperationOngoing: false, NextOperation: common.None}},
		{"filename8.txt", &FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.Delete}},
		{"filename9.txt", &FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.Copy}},
	}

	for _, val := range preCreactedValues {
		manager.filesStorage[val.fileName] = val.fileInfo
	}

	testValues := []struct {
		fileName         string
		keyName          string
		expectedFileInfo FileInfo
		expectedWork     *common.Work
	}{
		{
			"delete_2024-01-02T18:54:30+02:00_filename0.txt",
			"filename0.txt",
			FileInfo{PendingDeletion: true, OperationOngoing: true, NextOperation: common.None},
			&common.Work{FileName: "delete_2024-01-02T18:54:30+02:00_filename0.txt", Operation: common.Delete},
		},
		{
			"delete_2024-01-02T18:54:30+02:00_filename1.txt",
			"filename1.txt",
			FileInfo{PendingDeletion: true, OperationOngoing: true, NextOperation: common.Delete},
			nil,
		},
		{
			"delete_2024-01-02T18:54:30+02:00_filename2.txt",
			"filename2.txt",
			FileInfo{PendingDeletion: true, OperationOngoing: true, NextOperation: common.Delete},
			nil,
		},
		{
			"delete_2024-02-02T18:54:30+02:00_filename3.txt",
			"filename3.txt",
			FileInfo{PendingDeletion: true, OperationOngoing: false, NextOperation: common.None},
			nil,
		},
		{
			"delete_filename4.txt",
			"filename4.txt",
			FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.Delete},
			nil,
		},
		{
			"filename5.txt",
			"filename5.txt",
			FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.Copy},
			nil,
		},
		{
			"filename6.txt",
			"filename6.txt",
			FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.None},
			&common.Work{FileName: "filename6.txt", Operation: common.Copy},
		},
		{
			"delete_filename7.txt",
			"filename7.txt",
			FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.None},
			&common.Work{FileName: "delete_filename7.txt", Operation: common.Delete},
		},
		{
			"filename8.txt",
			"filename8.txt",
			FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.Delete},
			nil,
		},
		{
			"delete_filename9.txt",
			"filename9.txt",
			FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.Delete},
			nil,
		},
		{
			"delete_filename10.txt",
			"filename10.txt",
			FileInfo{PendingDeletion: false, OperationOngoing: true, NextOperation: common.None},
			&common.Work{FileName: "delete_filename10.txt", Operation: common.Delete},
		},
		{
			"delete_2124-01-02T18:54:30+02:00_filename11.txt",
			"filename11.txt",
			FileInfo{PendingDeletion: true, OperationOngoing: false, NextOperation: common.None},
			nil,
		},
	}

	for _, val := range testValues {
		manager.discernWork(val.fileName)

		checkFileInfo(t, &manager, val.keyName, val.expectedFileInfo)
		checkWorkFromChannel(t, &manager, val.expectedWork)
	}
}
