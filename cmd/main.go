package main

import (
	"flag"
	"fmt"
	"path/filepath"

	"backup/common"
)

var (
	hotDirPath     = flag.String("hot-dir", "./hotDir/", "Path to monitoring directory")
	backupDirPath  = flag.String("backup-dir", "./backupDir/", "Path to backup directory")
	viewMode       = flag.Bool("view-logs", false, "View existing logs")
	dateFilterVal  = flag.String("date-filter", "", "Filter logs by date(Format: yyyy/mm/dd)")
	timeFilterVal  = flag.String("time-filter", "", "Filter logs by time(Format: hh:mm:ss)")
	regexFilterVal = flag.String("regex-filter", "", "Regex to filter logs")
)

var appParams common.AppParams

func init() {
	flag.Parse()

	var err error

	appParams.HotDirPath, err = filepath.Abs(*hotDirPath)
	if err != nil {
		panic(fmt.Errorf("Error during getting absolute file path for " + *hotDirPath))
	}

	appParams.DestDirPath, err = filepath.Abs(*backupDirPath)
	if err != nil {
		panic(fmt.Errorf("Error during getting absolute file path for " + *backupDirPath))
	}

	return
}

func main() {
	if *viewMode {
		ViewLogs()
		return
	}

	RunBackup(appParams)
}
