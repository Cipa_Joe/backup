package observers

import (
	"io/fs"
	"testing"
	"time"

	"backup/common"
	"backup/logging"
)

type MockFileInfo struct {
	name    string
	modTime time.Time
}

func (mock MockFileInfo) Name() string {
	return mock.name
}

func (mock MockFileInfo) Size() int64 {
	return 1
}

func (mock MockFileInfo) Mode() fs.FileMode {
	return fs.ModeAppend
}

func (mock MockFileInfo) ModTime() time.Time {
	return mock.modTime
}

func (mock MockFileInfo) IsDir() bool {
	return false
}

func (mock MockFileInfo) Sys() interface{} {
	return nil
}

func MockStat(string) (fs.FileInfo, error) {
	return MockFileInfo{modTime: time.Now().Add(-time.Minute * 5)}, nil
}

func MockStatErrNotExist(string) (fs.FileInfo, error) {
	return nil, fs.ErrNotExist
}

func MockStatErrClosed(string) (fs.FileInfo, error) {
	return nil, fs.ErrClosed
}

func TestCheckForWork(t *testing.T) {
	Stat = MockStat

	logger := logging.CreateSilentLogger()
	incomingWorkChan := make(chan string, common.CHANNEL_SIZE)

	hotDirPath := "/test/hotDirectory"
	backupDirPath := "/test/backupDirectory"

	scanner := CreateDirectoryScanner(hotDirPath, backupDirPath, logger, incomingWorkChan)

	fileName := "delete_newFile.txt"
	fileInfo := MockFileInfo{name: fileName, modTime: time.Now()}
	scanner.checkForWork(fileInfo)

	if len(incomingWorkChan) == 1 {
		val := <-incomingWorkChan
		if val != fileName {
			t.Fatalf(`Wrong file name in channel, expected=%s`, fileName)
		}
	} else {
		t.Fatalf(`Wrong number operations in channel, number=%d`, len(incomingWorkChan))
	}

	fileName = "newFile.txt"
	fileInfo = MockFileInfo{name: fileName, modTime: time.Now()}
	scanner.checkForWork(fileInfo)

	if len(incomingWorkChan) == 1 {
		val := <-incomingWorkChan
		if val != fileName {
			t.Fatalf(`Wrong file name in channel, expected=%s`, fileName)
		}
	} else {
		t.Fatalf(`Wrong number operations in channel, number=%d`, len(incomingWorkChan))
	}

	fileName = "newFile2.txt"
	fileInfo = MockFileInfo{name: fileName, modTime: time.Now().Add(-time.Hour)}
	scanner.checkForWork(fileInfo)

	if len(incomingWorkChan) != 0 {
		t.Fatalf(`Wrong number operations in channel, number=%d`, len(incomingWorkChan))
	}

	Stat = MockStatErrNotExist

	fileName = "newFile3.txt"
	fileInfo = MockFileInfo{name: fileName, modTime: time.Now()}
	scanner.checkForWork(fileInfo)

	if len(incomingWorkChan) == 1 {
		val := <-incomingWorkChan
		if val != fileName {
			t.Fatalf(`Wrong file name in channel, expected=%s`, fileName)
		}
	} else {
		t.Fatalf(`Wrong number operations in channel, number=%d`, len(incomingWorkChan))
	}

	Stat = MockStatErrClosed

	fileName = "newFile4.txt"
	fileInfo = MockFileInfo{name: fileName, modTime: time.Now()}
	scanner.checkForWork(fileInfo)

	if len(incomingWorkChan) != 0 {
		t.Fatalf(`Wrong number operations in channel, number=%d`, len(incomingWorkChan))
	}
}
