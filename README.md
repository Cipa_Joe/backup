# Backup application

## Desciption

Application can be started in two different modes to choose from.

`Backup` mode is default:

* All new added files to hot diretory are copied to backup directory with the same name but `.bak` extension added to the end of file.
* All modifications of original files in hot directory cause rewriting of backedup files in backup directory.
* Scanning hot directory on application startup to handle all exsisting files.
* Rename original file ore create new file with prefix `delete_<filename>` causes deletion backedup `<filename>` file in both directories and `delete_<filename>` in hot directory.
* Rename original file with prefix `delete_<RFC3339>_<filename>` causes deletion backedup `<filename>` file and `delete_<RFC3339>_<filename>` in hot directory in specified time. (eg. `delete_2024-02-04T10:30:30+02:00_main.go`)

`View logs` mode:

* View all logs which application done in `logfile.log`.
* Filter logs by specified date.
* Filter logs by specified time.
* Filter logs by specified regex string.
* All filters can be applied together in different combinations.

## Usage

You can build and run application by using `task` runner. If you don't have it, you can visit [link](https://taskfile.dev/installation) to find a proper guide to install it.

```bash
# to build
> task build
# to create default directories
> task create_default_dirs
# to run application with default parameters
> task run
```

If you don't want to use `task` runner. You can build and run application in other way:

```bash
# to build
> go build -o backup cmd/*
# to see help
> ./backup -h
```

### Parameters

* `-backup-dir=<path>` - Path to backup directory (default `./backupDir/`)
* `-hot-dir=<path>` - Path to monitoring directory (default `./hotDir/`)
* `-view-logs` - View existing logs
* `-date-filter=<date>` - Filter logs by date(Format: `yyyy/mm/dd`)
* `-time-filter<time>` - Filter logs by time(Format: `hh:mm:ss`)
* `-regex-filter=<regex string>` - Regex string to filter logs

### Backup mode run example:

```bash
> ./backup -hot-dir="./testDir/hot" -backup-dir="./testDir/backup"
```

> `hot-dir` and `backup-dir` directories should already exsist.

### View logs mode run example:

```bash
> ./backup -view-logs -date-filter="2024/02/06" -time-filter="18:23:45" -regex-filter="INF"
```

`date-filter`, `time-filter`, `regex-filter` are optional parameters.

## Tesing

Run unit test:

```bash
> task tests
# or 
> go test ./...
