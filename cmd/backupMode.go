package main

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"backup/common"
	"backup/logging"
	"backup/workers"
	"backup/observers"
)

func RunBackup(appParams common.AppParams) {
	var wg sync.WaitGroup

	logger := logging.CreateFileLogger()
	logger.SetPrefix("BackupMode")
	defer logger.Close()

	logger.Info("====================================================")
	logger.Info("Start application in backup mode")
	logger.Info("Monitoring directory is " + appParams.HotDirPath)
	logger.Info("Backup directory is " + appParams.DestDirPath)

	ctx := createSignalHandler(logger)

	incomingWorkChan := make(chan string, common.CHANNEL_SIZE)

	manager := workers.CreateManager(logger, incomingWorkChan)
	manager.Start(ctx, &wg)

	for i := 0; i < common.WORKER_NUMBER; i++ {
		worker := manager.CreateWorker(appParams.HotDirPath, appParams.DestDirPath)
		worker.Start(ctx, &wg)
	}

	scanner := observers.CreateDirectoryScanner(appParams.HotDirPath, appParams.DestDirPath, logger, incomingWorkChan)
	scanner.Start()

	monitor := observers.CreateEventMonitor(appParams.HotDirPath, logger, incomingWorkChan)
	monitor.Start(ctx, &wg)

	wg.Wait()
}

func createSignalHandler(logger logging.FileLogger) context.Context {
	ctx, cancel := context.WithCancel(context.Background())
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		logger.Debug("Signal: " + sig.String() + ". Closing application" )
		cancel()
	}()

	return ctx
}
