package observers

import (
	"context"
	"os"
	"sync"

	"backup/logging"

	"github.com/fsnotify/fsnotify"
)

type EventMonitor struct {
	hotDirPath string
	logger     logging.FileLogger
	fsWatcher  *fsnotify.Watcher
	workChan   chan string
}

func CreateEventMonitor(hotDirPath string, logger logging.FileLogger, workChan chan string) EventMonitor {
	logger.SetPrefix("EventMonitor")
	return EventMonitor{
		hotDirPath: hotDirPath,
		logger:     logger,
		workChan:   workChan,
	}
}

func (monitor *EventMonitor) Start(ctx context.Context, wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		defer wg.Done()

		monitor.start(ctx)
	}()
}

func (monitor *EventMonitor) run(ctx context.Context) {
	for {
		select {
		case event, ok := <-monitor.fsWatcher.Events:
			if !ok {
				monitor.logger.Error("Got not ok response from fsWatcher")
				return
			}

			if event.Has(fsnotify.Create) || event.Has(fsnotify.Write) {
				fileInfo, err := os.Stat(event.Name)
				if err != nil {
					monitor.logger.Fatal("Getting file stat fatal: " + err.Error())
				}

				if fileInfo.Mode().IsRegular() {
					if event.Has(fsnotify.Create) {
						monitor.logger.Info("Detected creation of " + fileInfo.Name())
					} else {
						monitor.logger.Info("Detected modification of " + fileInfo.Name())
					}
					monitor.workChan <- fileInfo.Name()
				}
			}

		case <-ctx.Done():
			monitor.logger.Debug("EventMonitor is stopped")
			return
		}
	}
}

func (monitor *EventMonitor) start(ctx context.Context) {
	var err error
	monitor.fsWatcher, err = fsnotify.NewWatcher()
	if err != nil {
		monitor.logger.Fatal("EventMonitor fatal: " + err.Error())
	}

	defer monitor.fsWatcher.Close()

	err = monitor.fsWatcher.Add(monitor.hotDirPath)
	if err != nil {
		monitor.logger.Fatal("EventMonitor fatal: " + err.Error())
	}

	monitor.run(ctx)
}
