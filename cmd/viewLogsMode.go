package main

import (
	"backup/logging"
	"fmt"
)

func ViewLogs() {
	fmt.Println("Viewing logs mode: ")

	filters := []logging.Filter{}

	if dateFilter, err := logging.CreateDateFilter(*dateFilterVal); err != nil {
		fmt.Println(err.Error())
		return
	} else if dateFilter != nil {
		filters = append(filters, dateFilter)
	}

	if timeFilter, err := logging.CreateTimeFilter(*timeFilterVal); err != nil {
		fmt.Println(err.Error())
		return
	} else if timeFilter != nil {
		filters = append(filters, timeFilter)
	}

	if regexFilter, err := logging.CreateRegexFilter(*regexFilterVal); err != nil {
		fmt.Println(err.Error())
		return
	} else if regexFilter != nil {
		filters = append(filters, regexFilter)
	}

	logging.ViewLogs(filters)
}
