package workers

import (
	"context"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"

	"backup/common"
	"backup/logging"
)

var ReadFile = os.ReadFile
var WriteFile = os.WriteFile
var Remove = os.Remove

type Worker struct {
	hotDirPath  string
	destDirPath string
	resultChan  chan common.Work
	workChan    chan common.Work
	logger      logging.FileLogger
	regex       *regexp.Regexp
}

func (worker *Worker) Start(ctx context.Context, wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		defer wg.Done()

		worker.run(ctx)
	}()
}

func (worker *Worker) GetFileNames(fileName string) (string, string) {
	withoutPrefix := worker.GetFileWitoutPrefix(fileName)
	return filepath.Join(worker.hotDirPath, fileName), filepath.Join(worker.destDirPath, withoutPrefix+".bak")
}

func (worker *Worker) GetFileWitoutPrefix(fileName string) string {
	withoutPrefix := worker.regex.ReplaceAllString(fileName, "${3}")
	withoutPrefix = strings.ReplaceAll(withoutPrefix, "delete_", "")
	return withoutPrefix
}

func (worker *Worker) run(ctx context.Context) {
	for {
		select {
		case work := <-worker.workChan:
			worker.logger.Info("Received file to handle: " + work.FileName)
			worker.handleWork(work)
		case <-ctx.Done():
			worker.logger.Debug("Worker is stopped")
			return
		}
	}
}

func (worker *Worker) handleWork(work common.Work) {
	if work.Operation == common.Delete {
		worker.handleDelete(work.FileName)
	} else {
		worker.handleCopy(work.FileName)
	}

	worker.resultChan <- work
}

func (worker *Worker) handleDelete(fileName string) {
	originalFilePath, backupFilePath := worker.GetFileNames(fileName)
	fileWithoutPrefix := filepath.Join(worker.hotDirPath, worker.GetFileWitoutPrefix(fileName))

	err := deleteFiles([]string{originalFilePath, backupFilePath, fileWithoutPrefix})
	if err != nil {
		worker.logger.Warn(err.Error())
	}

	worker.logger.Info(fileWithoutPrefix + " is deleted from both directory")
	worker.logger.Info(fileName + " is deleted from hot directory")
}

func (worker *Worker) handleCopy(fileName string) {
	originalFilePath, backupFilePath := worker.GetFileNames(fileName)
	err := copyFile(originalFilePath, backupFilePath)
	if err != nil {
		worker.logger.Warn(err.Error())
	} else {
		worker.logger.Info(originalFilePath + " is backedup")
	}
}

func copyFile(originalFilePath, destinationFilePath string) error {
	content, err := ReadFile(originalFilePath)
	if err != nil {
		return err
	}

	err = WriteFile(destinationFilePath, content, 0644)
	if err != nil {
		return err
	}

	return nil
}

func deleteFiles(files []string) (err error) {
	for _, file := range files {
		someErr := Remove(file)
		if someErr != nil {
			err = someErr
		}
	}

	return err
}
