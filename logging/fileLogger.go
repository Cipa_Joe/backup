package logging

import (
	"backup/common"
	"fmt"
	"log"
	"os"
	"io"
)

type FileLogger struct {
	prefix  string
	fileLog *log.Logger
}

func CreateFileLogger() FileLogger {
	file, err := os.OpenFile(common.LOGFILE_NAME, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal("Can't open file for logger")
	}

	fileLog := log.New(file, "", log.LstdFlags)

	return FileLogger{fileLog: fileLog}
}

func CreateSilentLogger() FileLogger {
	fileLog := log.New(io.Discard, "", log.LstdFlags)
	return FileLogger{fileLog: fileLog}
}

func (logger *FileLogger) SetPrefix(prefix string) {
	logger.prefix = fmt.Sprintf("[%s]: ", prefix)
}

func (logger *FileLogger) Close() {
	if file, ok := logger.fileLog.Writer().(*os.File); ok {
		file.Close()
	}
}

func (logger *FileLogger) Debug(msg string) {
	logger.log("[DBG]", msg)
}

func (logger *FileLogger) Info(msg string) {
	logger.log("[INF]", msg)
}

func (logger *FileLogger) Warn(msg string) {
	logger.log("[WRN]", msg)
}

func (logger *FileLogger) Error(msg string) {
	logger.log("[ERR]", msg)
}

func (logger *FileLogger) Fatal(msg string) {
	logger.log("[FAT]", msg)
	log.Fatal(msg)
}

func (logger *FileLogger) log(severity, msg string) {
	logger.fileLog.Println(severity + logger.prefix + msg)
}
