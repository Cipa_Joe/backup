package common

import (
	"strings"
)

const (
	DELETE_REGEX_STR = "^(delete_)(\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[+-]\\d{2}:\\d{2})_(.*)$"
	DATE_REGEX_STR   = "\\d{4}/\\d{2}/\\d{2}"
	TIME_REGEX_STR   = "\\d{2}:\\d{2}:\\d{2}"
	LOGFILE_NAME     = "logfile.log"
	CHANNEL_SIZE     = 40
	WORKER_NUMBER    = 10
)

type AppParams struct {
	HotDirPath  string
	DestDirPath string
}

type WorkType int

const (
	None WorkType = iota
	Delete
	Copy
)

type Work struct {
	FileName  string
	Operation WorkType
}

func IsDeleteNeeded(filename string) bool {
	return strings.HasPrefix(filename, "delete_")
}
