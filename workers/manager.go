package workers

import (
	"context"
	"regexp"
	"strings"
	"sync"
	"time"

	"backup/common"
	"backup/logging"
)

type FileInfo struct {
	PendingDeletion  bool
	OperationOngoing bool
	NextOperation    common.WorkType
}

type Manager struct {
	incomingWorkChan chan string
	workerChan       chan common.Work
	resultChan       chan common.Work
	logger           logging.FileLogger
	regex            *regexp.Regexp
	filesStorage     map[string]*FileInfo
}

func CreateManager(logger logging.FileLogger, incomingWorkChan chan string) Manager {
	regex, err := regexp.Compile(common.DELETE_REGEX_STR)
	if err != nil {
		logger.Fatal("Invalid regex: " + err.Error())
	}

	workerChan := make(chan common.Work, common.CHANNEL_SIZE)
	resultChan := make(chan common.Work, common.CHANNEL_SIZE)

	logger.SetPrefix("Manager")

	manager := Manager{
		incomingWorkChan: incomingWorkChan,
		resultChan:       resultChan,
		workerChan:       workerChan,
		logger:           logger,
		regex:            regex,
		filesStorage:     make(map[string]*FileInfo),
	}

	return manager
}

func (manager *Manager) CreateWorker(hotDirPath, destDirPath string) Worker {
	logger := manager.logger
	logger.SetPrefix("Worker")
	return Worker{
		hotDirPath:  hotDirPath,
		destDirPath: destDirPath,
		resultChan:  manager.resultChan,
		workChan:    manager.workerChan,
		logger:      logger,
		regex:       manager.regex,
	}
}

func (manager *Manager) Start(ctx context.Context, wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		defer wg.Done()

		manager.run(ctx)
	}()
}

func (manager *Manager) run(ctx context.Context) {
	for {
		select {
		case fileName := <-manager.incomingWorkChan:
			manager.discernWork(fileName)
		case result := <-manager.resultChan:
			manager.handleResult(result)
		case <-ctx.Done():
			manager.logger.Debug("Manager is stopped")
			return
		}
	}
}

func (manager *Manager) discernWork(fileName string) {
	manager.logger.Info("Start handling " + fileName)
	keyName := manager.getNameWithoutPrefix(fileName)

	fileInfo, ok := manager.filesStorage[keyName]

	if !ok {
		manager.createNewFileInfo(fileName)
		fileInfo = manager.filesStorage[keyName]
	}

	if (fileInfo.PendingDeletion) {
		manager.logger.Info(fileName + " is planned to be deleted. Current action will be skipped")
		return
	}

	if manager.isPendingDeleteNeeded(fileName) {
		manager.handlePendingDelete(fileInfo, fileName)
		return
	}

	workType := manager.determineWorkType(fileName, keyName)
	manager.sendWork(fileInfo, fileName, workType)
}

func (manager *Manager) handleResult(result common.Work) {
	manager.logger.Debug("Received result for " + result.FileName)

	keyName := manager.getNameWithoutPrefix(result.FileName)

	fileInfo, ok := manager.filesStorage[keyName]

	if result.Operation == common.Delete {
		delete(manager.filesStorage, keyName)
		return
	}

	if !ok {
		manager.logger.Fatal("Inconsistence storage")
	}

	if fileInfo.NextOperation != common.None {
		manager.logger.Debug("Take next operation from queue for " + result.FileName)
		manager.workerChan <- common.Work{FileName: result.FileName, Operation: fileInfo.NextOperation}
		fileInfo.NextOperation = common.None
		fileInfo.OperationOngoing = true
	} else {
		manager.logger.Info("Handling " + result.FileName + " is finished")
		fileInfo.OperationOngoing = false
	}
}

func (manager *Manager) handlePendingDelete(fileInfo *FileInfo, fileName string) {
	manager.logger.Info(fileName + " has prefix for pending deletion")
	fileInfo.PendingDeletion = true

	duration := manager.calculateDuration(fileName)

	if duration <= 0 {
		manager.logger.Info("Start deletion of " + fileName + " right now due to time was set in the past")
		manager.sendWork(fileInfo, fileName, common.Delete)
		return
	}

	go manager.createDeleteTimer(duration, fileName)
}

func (manager *Manager) createDeleteTimer(duration time.Duration, fileName string) {
	timer := time.NewTicker(duration)

	defer timer.Stop()
	for range timer.C {
			manager.logger.Info("Timer is fired. Start deletion of " + fileName)
			manager.workerChan <- common.Work{FileName: fileName, Operation: common.Delete}
			return
	}
}

func (manager *Manager) createNewFileInfo(fileName string) {
	manager.logger.Debug("Create FileInfo for new file " + fileName)

	keyName := manager.getNameWithoutPrefix(fileName)

	manager.filesStorage[keyName] = &FileInfo{
		PendingDeletion:  false,
		OperationOngoing: false,
		NextOperation:    common.None}
}

func (manager *Manager) sendWork(fileInfo *FileInfo, fileName string, workType common.WorkType) {
	if fileInfo.OperationOngoing {
		if (workType == common.Delete) {
			manager.logger.Info("Some operation is ongoing with this file. Rewrite next operation to Delete.")
			fileInfo.NextOperation = common.Delete
		} else if (fileInfo.NextOperation == common.None) {
			manager.logger.Info("Some operation is ongoing with this file. Wait until it's finished.")
			fileInfo.NextOperation = workType
		}
	} else {
		fileInfo.OperationOngoing = true
		manager.logger.Debug("Send operation to worker")
		manager.workerChan <- common.Work{FileName: fileName, Operation: workType}
	}
}

func (manager *Manager) determineWorkType(fileName, keyName string) (workType common.WorkType) {
	workType = common.Copy

	if common.IsDeleteNeeded(fileName) {
		manager.logger.Info(keyName + " deletion detected")
		workType = common.Delete
	}

	return
}

func (manager *Manager) getNameWithoutPrefix(fileName string) string {
	cutFileName := manager.regex.ReplaceAllString(fileName, "${3}")
	return strings.ReplaceAll(cutFileName, "delete_", "")
}

func (manager *Manager) isPendingDeleteNeeded(fileName string) bool {
	return manager.regex.MatchString(fileName)
}

func (manager *Manager) calculateDuration(fileName string) time.Duration {
	matches := manager.regex.FindStringSubmatch(fileName)
	var deleteTime time.Time
	var err error

	if len(matches) >= 3 {
		deleteTime, err = time.Parse(time.RFC3339, matches[2])
	}

	if err != nil || len(matches) < 3 {
		manager.logger.Error("Can't correctly parse time from the file name. Will be deleted without pending")
		deleteTime = time.Now()
	}

	return time.Until(deleteTime)
}
