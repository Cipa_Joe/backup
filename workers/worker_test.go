package workers

import (
	"os"
	"testing"

	"backup/common"
	"backup/logging"
)

func TestGetFileNames(t *testing.T) {
	logger := logging.CreateSilentLogger()
	incomingWorkChan := make(chan string, common.CHANNEL_SIZE)
	manager := CreateManager(logger, incomingWorkChan)

	hotDirPath := "/test/hotDirectory"
	backupDirPath := "/test/backupDirectory"

	worker := manager.CreateWorker(hotDirPath, backupDirPath)

	fileNames := []struct {
		original string
		backedup string
	}{
		{"filename1.hpp", "filename1.hpp.bak"},
		{"delete_filename2.cpp", "filename2.cpp.bak"},
		{"delete_2024-02-02T18:54:30+02:00_filename3.txt", "filename3.txt.bak"},
	}

	for _, val := range fileNames {
		originalFileWant := hotDirPath + "/" + val.original
		backedupFileWant := backupDirPath + "/" + val.backedup

		originalFile, backedupFile := worker.GetFileNames(val.original)

		if originalFile != originalFileWant {
			t.Fatalf(`original file name isn't valid, actual=%s, expected=%s`, originalFile, originalFileWant)
		}

		if backedupFile != backedupFileWant {
			t.Fatalf(`backedup file name isn't valid, actual=%s, expected=%s`, backedupFile, backedupFileWant)
		}
	}
}

type MockStruct struct {
	isReadFileCalled  bool
	isWriteFileCalled bool
	isRemoveCalled    bool
}

func (mock *MockStruct) MockReadFile(string) ([]byte, error) {
	mock.isReadFileCalled = true
	return nil, nil
}

func (mock *MockStruct) MockWriteFile(string, []byte, os.FileMode) error {
	mock.isWriteFileCalled = true
	return nil
}

func (mock *MockStruct) MockRemove(string) error {
	mock.isRemoveCalled = true
	return nil
}

func TestHandleWork(t *testing.T) {
	logger := logging.CreateSilentLogger()
	incomingWorkChan := make(chan string, common.CHANNEL_SIZE)
	manager := CreateManager(logger, incomingWorkChan)

	hotDirPath := "/test/hotDirectory"
	backupDirPath := "/test/backupDirectory"

	worker := manager.CreateWorker(hotDirPath, backupDirPath)

	mock := MockStruct{false, false, false}
	ReadFile = mock.MockReadFile
	WriteFile = mock.MockWriteFile
	Remove = mock.MockRemove

	fileName := "fileName.go"

	worker.handleWork(common.Work{FileName: fileName, Operation: common.Copy})

	if len(worker.resultChan) == 1 {
		actualResult := <-worker.resultChan

		if actualResult.FileName != fileName {
			t.Errorf(`FileName actual=%s, expected=%s`, actualResult.FileName, fileName)
		}

		if actualResult.Operation != common.Copy {
			t.Errorf(`not Copy operation`)
		}

		if mock.isReadFileCalled != true {
			t.Errorf(`ReadFile method wasn't called`)
		}

		if mock.isWriteFileCalled != true {
			t.Errorf(`WriteFile method wasn't called`)
		}

		if mock.isRemoveCalled != false {
			t.Errorf(`WriteFile method wasn't called`)
		}
	} else {
		t.Fatalf(`Wrong number operations in channel, number=%d`, len(manager.workerChan))
	}

	worker.handleWork(common.Work{FileName: fileName, Operation: common.Delete})

	if len(worker.resultChan) == 1 {
		actualResult := <-worker.resultChan

		if actualResult.FileName != fileName {
			t.Errorf(`FileName actual=%s, expected=%s`, actualResult.FileName, fileName)
		}

		if actualResult.Operation != common.Delete {
			t.Errorf(`not Delete operation`)
		}

		if mock.isRemoveCalled != true {
			t.Errorf(`WriteFile method wasn't called`)
		}
	}
}
