package logging

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	"backup/common"
)

func ViewLogs(filters []Filter) {
	file, err := os.Open(common.LOGFILE_NAME)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Text()
		isFiltersPassed := true

		for _, filter := range filters {
			if !filter.isPassed(line) {
				isFiltersPassed = false
				break
			}
		}

		if isFiltersPassed {
			fmt.Println(line)
		}
	}
}

type Filter interface {
	isPassed(string) bool
}

func isRegexPassed(regexStr, value string) bool {
	regex, err := regexp.Compile(regexStr)
	if err != nil {
		log.Fatal("Invalid regex: " + err.Error())
	}

	return regex.MatchString(value)
}

type DateFilter struct {
	date string
}

func CreateDateFilter(date string) (Filter, error) {
	if date == "" {
		return nil, nil
	}

	if !isRegexPassed(common.DATE_REGEX_STR, date) {
		return nil, fmt.Errorf("Date format isn't valid")
	}

	return &DateFilter{date: date}, nil
}

func (dateFilter *DateFilter) isPassed(line string) bool {
	return strings.Contains(line, dateFilter.date)
}

type TimeFilter struct {
	time string
}

func CreateTimeFilter(time string) (Filter, error) {
	if time == "" {
		return nil, nil
	}

	if !isRegexPassed(common.TIME_REGEX_STR, time) {
		return nil, fmt.Errorf("Time format isn't valid")
	}

	return &TimeFilter{time: time}, nil
}

func (timeFilter *TimeFilter) isPassed(line string) bool {
	return strings.Contains(line, timeFilter.time)
}

type RegexFilter struct {
	regex *regexp.Regexp
}

func CreateRegexFilter(regexStr string) (Filter, error) {
	if regexStr == "" {
		return nil, nil
	}

	regex, err := regexp.Compile(regexStr)
	if err != nil {
		log.Fatal("Invalid regex: " + err.Error())
	}

	return &RegexFilter{regex: regex}, nil
}

func (regexFilter *RegexFilter) isPassed(line string) bool {
	return regexFilter.regex.MatchString(line)
}
