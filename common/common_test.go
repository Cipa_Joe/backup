package common

import (
	"testing"
)

func TestIsDeleteNeeded(t *testing.T) {
	
	if IsDeleteNeeded("filename.cpp") != false {
		t.Fatalf(`IsDeleteNeeded result actual=true, expected=false`)
	}

	if IsDeleteNeeded("Delete_filename.cpp") != false {
		t.Fatalf(`IsDeleteNeeded result actual=true, expected=false`)
	}

	if IsDeleteNeeded("DELETE_filename.cpp") != false {
		t.Fatalf(`IsDeleteNeeded result actual=true, expected=false`)
	}

	if IsDeleteNeeded("deletefilename.cpp") != false {
		t.Fatalf(`IsDeleteNeeded result actual=true, expected=false`)
	}

	if IsDeleteNeeded("delete_filename.cpp") != true {
		t.Fatalf(`IsDeleteNeeded result actual=false, expected=true`)
	}
}
